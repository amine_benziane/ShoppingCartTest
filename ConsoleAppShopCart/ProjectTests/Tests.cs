﻿using NUnit.Framework;
namespace ProjectTests
{
    [TestFixture]
    public class Tests
    {

        [Test]
        public void test1()
        {
            var x = 2;
            var y = 1;

            var z = x + y;

            Assert.AreEqual(3, z);
        }

        [Test]
        public void test2()
        {
            var x = 2;
            var y = 2;

            var z = x + y;

            Assert.AreEqual(4, z);
        }
    }
}
